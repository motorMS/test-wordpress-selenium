import allure
import pytest
from selene.support.shared import SharedBrowser, SharedConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from pages.login_page import LoginPage
from pages.main_page import MainPage


@pytest.fixture(scope="function")
def selene() -> SharedBrowser:
    options = Options()
    # options.add_argument("--headless")
    # options.add_argument('window-size=1920,1080')
    options.add_argument("--start-maximized")
    global wd
    wd = webdriver.Chrome(options=options, executable_path='/home/motor/work/drivers/chromedriver')
    selene = SharedBrowser(SharedConfig(driver=wd, timeout=5, base_url='https://s1.demo.opensourcecms.com/wordpress/'))
    yield selene
    selene.quit()


@pytest.fixture(scope="function")
def login(selene):
    main_page = MainPage(selene)
    main_page.on_main_page()
    main_page.goto_login_page()
    login_page = LoginPage(selene)
    login_page.input_login('opensourcecms')
    login_page.input_password('opensourcecms')
    login_page.click_login_btn()
    yield selene


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport():
    outcome = yield
    rep = outcome.get_result()
    if rep.when == 'call' and rep.failed:
        try:
            allure.attach(wd.get_screenshot_as_png(), name='screenshot', attachment_type=allure.attachment_type.PNG)
        except Exception as e:
            print(f'Fail to take screen-shot: {e}')
