import allure
from selene import be
from pages.base_page import BasePage
from pages.locators import MainPageLocators


class MainPage(BasePage):
    def on_main_page(self):
        with allure.step('Открытие главной станицы'):
            self.selene.open('/')

    def goto_login_page(self):
        with allure.step('Клик по ссылке "Log in"'):
            self.selene.element(MainPageLocators.LOGIN_LINK).click()

    def click_test_category(self):
        with allure.step('Клик по по тестовой категории. Фильтрация'):
            self.selene.element(MainPageLocators.TEST_CATEGORIY).click()

    def check_added_post(self):
        with allure.step('Наличие добавленного поста'):
            self.selene.element(MainPageLocators.TEST_POST_TITLE).should(be.present)
