import allure
from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from pages.locators import LoginPageLocators


class LoginPage(BasePage):
    def input_login(self, login):
        with allure.step('Ввод логина'):
            image = self.selene.driver.find_element(By.XPATH, LoginPageLocators.LOGIN_INPUT)
            self.selene.driver.execute_script(f'arguments[0].value = "{login}"', image)

    def input_password(self, password):
        with allure.step('Ввод пароля'):
            image = self.selene.driver.find_element(By.XPATH, LoginPageLocators.PASSWORD_INPUT)
            self.selene.driver.execute_script(f'arguments[0].value = "{password}"', image)

    def click_login_btn(self):
        with allure.step('Клик по кнопке "Log in"'):
            self.selene.element(LoginPageLocators.LOGIN_BTN).hover().click()
