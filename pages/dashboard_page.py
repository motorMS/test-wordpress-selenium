import allure
from selene import be
from pages.base_page import BasePage
from pages.locators import DashboardPageLocators


class DashboardPage(BasePage):
    def hover_post(self):
        with allure.step('Наведение на Posts'):
            self.selene.element(DashboardPageLocators.POST_BTN).hover()

    def click_categories(self):
        with allure.step('Клик по Categories'):
            self.selene.element(DashboardPageLocators.CATEGORIES_BTN).click()

    def input_category_name(self, name):
        with allure.step('Ввод имени категории'):
            self.selene.element(DashboardPageLocators.NEW_CATEGORY_NAME_INPUT).type(name)

    def click_add_new_category_btn(self):
        with allure.step('Клик по кнопке "Add New Category"'):
            self.selene.element(DashboardPageLocators.ADD_NEW_CATEGORY_BTN).click()

    def input_search_category_name(self, name):
        with allure.step('Ввод названия категории'):
            self.selene.element(DashboardPageLocators.SEARCH_CATEGORY_INPUT).type(name)

    def click_search_category_btn(self):
        with allure.step('Клик по кнопке "Search Categories"'):
            self.selene.element(DashboardPageLocators.SEARCH_CATEGORIES_BTN).click()

    def check_added_category(self):
        with allure.step('Наличие добавленной категории'):
            self.selene.element(DashboardPageLocators.ADDED_CATEGORY).should(be.clickable)

    def click_add_new(self):
        with allure.step('Клик по кнопке "Add New"'):
            self.selene.element(DashboardPageLocators.ADD_NEW_BTN).click()

    def input_post_name(self, name):
        with allure.step('Ввод названия поста'):
            self.selene.element(DashboardPageLocators.POST_NAME_INPUT).type(name)

    def input_post_description(self, desc):
        with allure.step('Ввод содержимого'):
            self.selene.element(DashboardPageLocators.POST_DESCRIPTION_INPUT).click()
            self.selene.element(DashboardPageLocators.POST_DESCRIPTION_INPUT_WAIT).type(desc)
            self.selene.element(DashboardPageLocators.POST_NAME_INPUT).click()

    def close_popup(self):
        with allure.step('Закрытие всплывающего окна'):
            self.selene.element(DashboardPageLocators.POPUP_BTN).click()

    def select_categories_list(self):
        with allure.step('Клик по списку категорий'):
            self.selene.element(DashboardPageLocators.CATEGORIES_LIST_BTN).click()

    def select_test_checkbox(self):
        with allure.step('Клик по чеекбоксу тестовой категории'):
            self.selene.element(DashboardPageLocators.CATEGORIES_TEST_CHECKBOX).click()

    def click_publich_btn(self, loc):
        with allure.step('Клик по кнопке "Publish"'):
            self.selene.element(loc).click()

    def hover_menu_btn(self):
        with allure.step('Наведение на кнопку Меню'):
            self.selene.element(DashboardPageLocators.MENU_BTN).hover()

    def click_visit_site_btn(self):
        with allure.step('Клик по кнопке "Visit Site"'):
            self.selene.element(DashboardPageLocators.VISIT_SITE_BTN).click()
