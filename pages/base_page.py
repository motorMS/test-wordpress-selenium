from selene.support.shared import SharedBrowser


class BasePage:
    def __init__(self, selene: SharedBrowser):
        self.selene = selene
