class MainPageLocators:
    LOGIN_LINK = '//a[@href="https://s1.demo.opensourcecms.com/wordpress/wp-login.php"]'
    TEST_CATEGORIY = '//a[@href="https://s1.demo.opensourcecms.com/wordpress/category/36484/"]'
    TEST_POST_TITLE = '//section[@id="primary"]//a[text()="Test Post"]'


class LoginPageLocators:
    LOGIN_INPUT = '//input[@type="text"]'
    PASSWORD_INPUT = '//input[@type="password"]'
    LOGIN_BTN = '//input[@class="button button-primary button-large"]'
    LOGIN_ERROR = '//div[@id="login_error"]'


class DashboardPageLocators:
    POST_BTN = '//li[@id="menu-posts"]//div[@class="wp-menu-name"]'
    CATEGORIES_BTN = '//a[@href="edit-tags.php?taxonomy=category"]'
    NEW_CATEGORY_NAME_INPUT = '//input[@name="tag-name"]'
    ADD_NEW_CATEGORY_BTN = '//input[@value="Add New Category"]'
    SEARCH_CATEGORY_INPUT = '//input[@type="search"]'
    SEARCH_CATEGORIES_BTN = '//input[@id="search-submit"]'
    ADDED_CATEGORY = '//a[@aria-label="“36484” (Edit)"]'
    ADD_NEW_BTN = '//a[@href="post-new.php"]'
    POST_NAME_INPUT = '//textarea[@id="post-title-0"]'
    POST_DESCRIPTION_INPUT = '//textarea[@role="button"]'
    POST_DESCRIPTION_INPUT_WAIT = '//p[@role="textbox"]'
    POPUP_BTN = '//button[@aria-label="Disable tips"]'
    CATEGORIES_LIST_BTN = '//button[text()="Categories"]'
    CATEGORIES_TEST_CHECKBOX = '//label[text()="36484"]'
    PUBLISH_BTN = '//button[@class="components-button editor-post-publish-panel__toggle is-button is-primary"]'
    PUBLISH_SUBMIT_BTN = '//button[@class="components-button editor-post-publish-button is-button is-default is-primary is-large"]'
    MENU_BTN = '//li[@id="wp-admin-bar-site-name"]'
    VISIT_SITE_BTN = '//li[@id="wp-admin-bar-view-site"]'
