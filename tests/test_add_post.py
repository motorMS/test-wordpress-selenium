import allure
from pages.dashboard_page import DashboardPage
from pages.locators import DashboardPageLocators
from pages.main_page import MainPage


@allure.suite('Логин')
@allure.title('Добавление поста')
def test_add_post(login):
    dashboard_page = DashboardPage(login)
    dashboard_page.hover_post()
    dashboard_page.click_add_new()
    dashboard_page.close_popup()
    dashboard_page.input_post_name('Test Post')
    dashboard_page.input_post_description('This Is Test Post')
    dashboard_page.select_categories_list()
    dashboard_page.select_test_checkbox()
    dashboard_page.click_publich_btn(DashboardPageLocators.PUBLISH_BTN)
    dashboard_page.click_publich_btn(DashboardPageLocators.PUBLISH_SUBMIT_BTN)
    dashboard_page.hover_menu_btn()
    dashboard_page.click_visit_site_btn()
    main_page = MainPage(login)
    main_page.click_test_category()
    main_page.check_added_post()
