import allure
from selene import have
from pages.login_page import LoginPage
from pages.main_page import MainPage


@allure.suite('Логин')
@allure.title('Валидный логин')
def test_open_main_page(selene):
    main_page = MainPage(selene)
    main_page.on_main_page()
    main_page.goto_login_page()
    login_page = LoginPage(selene)
    login_page.input_login('opensourcecms')
    login_page.input_password('opensourcecms')
    login_page.click_login_btn()
    with allure.step('Переход на страницу Dashboard'):
        selene.should(have.title('Dashboard ‹ opensourcecms — WordPress'))
