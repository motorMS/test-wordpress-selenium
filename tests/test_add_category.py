import allure
from pages.dashboard_page import DashboardPage


@allure.suite('Логин')
@allure.title('Добавление категории')
def test_add_category(login):
    dashboard_page = DashboardPage(login)
    dashboard_page.hover_post()
    dashboard_page.click_categories()
    dashboard_page.input_category_name('36484')
    dashboard_page.click_add_new_category_btn()
    dashboard_page.input_search_category_name('36484')
    dashboard_page.click_search_category_btn()
    dashboard_page.check_added_category()
