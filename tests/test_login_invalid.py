import allure
from selene import be
from pages.locators import LoginPageLocators
from pages.login_page import LoginPage
from pages.main_page import MainPage


@allure.suite("Логин")
@allure.title("Невалидный логин")
def test_open_main_page(selene):
    main_page = MainPage(selene)
    main_page.on_main_page()
    main_page.goto_login_page()
    login_page = LoginPage(selene)
    login_page.input_login('invalid')
    login_page.input_password('invalid')
    login_page.click_login_btn()
    with allure.step('Появление ошибки - ERROR: Invalid username.'):
        selene.element(LoginPageLocators.LOGIN_ERROR).should(be.present)
